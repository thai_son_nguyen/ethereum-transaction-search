﻿using EtheriumTransactionSearch.Domain.Queries;
using FluentAssertions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xunit;

namespace EtheriumTransactionSearch.Domain.Test.Queries
{
    public class BlockTransactionsQueryTests
    {
        public class Validation
        {
            [Theory]
            [InlineData("")]
            [InlineData(null)]
            public void WhenBlockNumberIsNotGivenShouldHaveError(string blockNumber)
            {
                var query = new BlockTransactionsQuery
                {
                    BlockNumber = blockNumber
                };
                var expectedErrors = new List<string> { "Block number is required" };

                var actualErrors = new List<ValidationResult>();
                Validator.TryValidateObject(query, new ValidationContext(query), actualErrors);

                expectedErrors.Should().Equal(actualErrors.Select(e => e.ErrorMessage));
            }

            [Theory]
            [InlineData("a")]
            [InlineData("asdsadbc")]
            public void WhenBlockNumberIsGivenShouldNotHaveError(string blockNumber)
            {
                var query = new BlockTransactionsQuery
                {
                    BlockNumber = blockNumber
                };

                var actualErrors = new List<ValidationResult>();
                Validator.TryValidateObject(query, new ValidationContext(query), actualErrors);

                actualErrors.Should().BeEmpty();
            }
        }
    }
}
