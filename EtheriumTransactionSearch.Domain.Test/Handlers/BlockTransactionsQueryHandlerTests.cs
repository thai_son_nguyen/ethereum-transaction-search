﻿using EtheriumTransactionSearch.Domain.Errors;
using EtheriumTransactionSearch.Domain.Handlers;
using EtheriumTransactionSearch.Domain.Queries;
using FluentAssertions;
using Moq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EtheriumTransactionSearch.Domain.Test.Handlers
{
    public class BlockTransactionsQueryHandlerTests
    {
        public class ValidationQuery
        {
            [Fact]
            public void WhenThereIsErrorShouldRaiseError()
            {
                var query = new BlockTransactionsQuery();

                var expectedErrors = new List<ValidationResult>();
                var valid = Validator.TryValidateObject(query, new ValidationContext(query), expectedErrors);

                var handler = new BlockTransactionsQueryHandler(new Mock<IRepository<Block, string>>().Object);
                
                handler.Awaiting(h => h.Execute(query)).Should().Throw<DomainError>().And.Errors.Should().Equal(expectedErrors.Select(e => e.ErrorMessage));                
            }
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task ShouldUseRepositoryToFindBlockTransactions(string address)
        {
            var query = new BlockTransactionsQuery
            {
                BlockNumber = "abc",
                Address = address
            };

            var block = new Block
            {
                Transactions = new List<BlockTransaction>
                {
                    new BlockTransaction
                    {
                        BlockNumber = "1",
                        From = "asdasd",
                        To = "asadasdsdasd"
                    }
                }
            };

            var repo = new Mock<IRepository<Block, string>>();
            repo.Setup(r => r.Get(query.BlockNumber)).ReturnsAsync(block);

            var handler = new BlockTransactionsQueryHandler(repo.Object);

            var result = await handler.Execute(query);

            result.Should().Equal(block.Transactions);
        }

        [Fact]
        public async Task WhenThereIsNoBlockFoundShouldReturnEmptyList()
        {
            var query = new BlockTransactionsQuery
            {
                BlockNumber = "abc"
            };
           
            var repo = new Mock<IRepository<Block, string>>();
            repo.Setup(r => r.Get(query.BlockNumber)).ReturnsAsync((Block) null);

            var handler = new BlockTransactionsQueryHandler(repo.Object);

            var result = await handler.Execute(query);

            result.Should().BeEmpty();
        }

        [Fact]
        public async Task WhenThereIsAddressFilterShouldFilterResultByAddress()
        {
            var query = new BlockTransactionsQuery
            {
                BlockNumber = "abc",
                Address = "ddd"
            };

            var block = new Block
            {
                Transactions = new List<BlockTransaction>
                {
                    new BlockTransaction
                    {
                        BlockNumber = "1",
                        From = "eee",
                        To = "fff"
                    },
                    new BlockTransaction
                    {
                        BlockNumber = "2",
                        From = query.Address,
                        To = "qqq"
                    },
                    new BlockTransaction
                    {
                        BlockNumber = "3",
                        From = "xxx",
                        To = query.Address
                    }
                }
            };

            var repo = new Mock<IRepository<Block, string>>();
            repo.Setup(r => r.Get(query.BlockNumber)).ReturnsAsync(block);

            var handler = new BlockTransactionsQueryHandler(repo.Object);

            var result = await handler.Execute(query);

            result.Should().Equal(block.Transactions.Where(t => t.From == query.Address || t.To == query.Address).ToList());
        }
    }
}
