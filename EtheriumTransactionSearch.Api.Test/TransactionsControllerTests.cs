using EtheriumTransactionSearch.Api.Controllers;
using EtheriumTransactionSearch.Domain;
using EtheriumTransactionSearch.Domain.Handlers;
using EtheriumTransactionSearch.Domain.Queries;
using FluentAssertions;
using FluentAssertions.Execution;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace EtheriumTransactionSearch.Api.Test
{
    public class TransactionsControllerTests
    {
        public class GetTransactions {

            private Mock<IQueryHandler<BlockTransactionsQuery, List<BlockTransaction>>> mockQueryHandler;
            private TransactionsController controller;

            public GetTransactions()
            {
                mockQueryHandler = new Mock<IQueryHandler<BlockTransactionsQuery, List<BlockTransaction>>>();
                controller = new TransactionsController(mockQueryHandler.Object);
            }
            
            [Fact]
            public async Task ShouldUseHandlerToExecuteQuery()
            {
                var blockNumber = "111";
                var address = "asdadas";
                var expected = new List<BlockTransaction>
                {
                    new BlockTransaction
                    {
                        BlockNumber = "1",
                        From = "asdasd",
                        To = "asadasdsdasd"
                    }
                };

                mockQueryHandler.Setup(h => h.Execute(It.Is<BlockTransactionsQuery>(q => q.BlockNumber == blockNumber && q.Address == address))).ReturnsAsync(expected);

                var response = await controller.SearchTransactions(blockNumber, address);

                using(new AssertionScope())
                {
                    response.Should().BeOfType<OkObjectResult>();
                    var value = ((OkObjectResult)response).Value;
                    value.Should().BeOfType<List<BlockTransaction>>();
                    ((List<BlockTransaction>)value).Should().BeEquivalentTo(expected);
                }
            }
        }        
    }
}
