﻿using EtheriumTransactionSearch.Domain;
using EtheriumTransactionSearch.Domain.Errors;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EtheriumTransactionSearch.Repository
{
    public class InfuraRepository : IRepository<Block, string>
    {    
        private HttpClient httpClient;
        private string url;

        public InfuraRepository(HttpClient httpClient)
        {
            this.httpClient = httpClient;
            this.url = httpClient.BaseAddress.OriginalString;
            this.httpClient.BaseAddress = null;
        }

        public async Task<Block> Get(string key)
        {
            var request = JObject.FromObject(new
            {
                jsonrpc = "2.0",
                method = "eth_getBlockByNumber",
                Params = new List<object> { key, true },
                id = 1
            });

            try
            {
                var response = await httpClient.PostAsync(url, new StringContent(request.ToString(), Encoding.UTF8, "application/json"));

                var responseData = JsonConvert.DeserializeObject<GetBlockByNumberResponse>(await response.Content.ReadAsStringAsync());

                return responseData.Result;
            } 
            catch
            {
                throw new DomainError(new List<string> { "Failed to load data" });
            }
        }
    }
}
