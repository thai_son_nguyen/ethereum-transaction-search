﻿using EtheriumTransactionSearch.Domain;

namespace EtheriumTransactionSearch.Repository
{
    public class GetBlockByNumberResponse
    {
        public Block Result { get; set; }
    }
}
