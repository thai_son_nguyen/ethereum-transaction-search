## SYSTEM REQUIREMENTS
1. [.NET Core 3.1](https://dotnet.microsoft.com/download/dotnet/3.1)
2. [Node JS](https://nodejs.org/en/download/)
3. Tested in Windows

## DEVELOPMENT TOOL
1. [MS Visual Studio 2019](https://visualstudio.microsoft.com/downloads/)
---
## RUN INSTRUCTIONS
1. Clone the repo
2. Open the solution in Visual Studio
3. Select startup project as `EtheriumTransactionSearch.Api`
4. Run
5. Both React client app and web api will be run together
---
## ASSUMPTIONS

#### APPLICATION BEHAVIOURS
1. For convenience, the Infura Api path is configurable via entry `InfuraUrl` in `appsettings.json`
2. Block number is mandatory to search
3. Address is optional

#### NOT IN SCOPE
1. Authentication and Authorization
2. Logging
3. Performance tuning
4. Greate UX
5. Amazing User Interface
6. Tests for UI
---
## DESIGN CHOICES
1. Minimal error handling which means that exception will be thrown and error will be displayed to UI
2. The architecture could be over-engineer but it is extendable and for demonstration purpose.
3. For simplicity, Web API and React Client template was used to create the solution
4. The UI is very robust
---
## FUTURE IMPROVEMENT
1. Extract Infura Api path to env variable for security reason
2. Authentication and Authorization
3. Logging
4. Split client app from Web API
