﻿using System;
using System.Collections.Generic;

namespace EtheriumTransactionSearch.Domain.Errors
{
    public class DomainError : Exception
    {
        public List<string> Errors { get; private set; }

        public DomainError(List<string> errors)
        {
            Errors = errors;
        }
    }
}
