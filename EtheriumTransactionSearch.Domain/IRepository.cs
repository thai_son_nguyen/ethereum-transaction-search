﻿using System.Threading.Tasks;

namespace EtheriumTransactionSearch.Domain
{
    public interface IRepository<TEntity, TKey>
    {
        Task<TEntity> Get(TKey key);
    }
}
