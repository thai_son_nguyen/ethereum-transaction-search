﻿using System.ComponentModel.DataAnnotations;

namespace EtheriumTransactionSearch.Domain.Queries
{
    public class BlockTransactionsQuery
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Block number is required")]
        public string BlockNumber { get; set; }
        public string Address { get; set; }
    }
}
