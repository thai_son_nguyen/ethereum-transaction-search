﻿using EtheriumTransactionSearch.Domain.Errors;
using EtheriumTransactionSearch.Domain.Queries;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EtheriumTransactionSearch.Domain.Handlers
{
    public class BlockTransactionsQueryHandler : IQueryHandler<BlockTransactionsQuery, List<BlockTransaction>>
    {
        private IRepository<Block, string> repository;

        public BlockTransactionsQueryHandler(IRepository<Block, string> repository)
        {
            this.repository = repository;
        }

        public async Task<List<BlockTransaction>> Execute(BlockTransactionsQuery query)
        {
            EnsureValid(query);

            var block = await repository.Get(query.BlockNumber);
            var transactions = block?.Transactions ?? new List<BlockTransaction>();

            if (string.IsNullOrWhiteSpace(query.Address))
            {
                return transactions;
            }
            return transactions.Where(t => t.From == query.Address || t.To == query.Address).ToList();
        }

        private void EnsureValid(BlockTransactionsQuery query)
        {
            var expectedErrors = new List<ValidationResult>();
            if (!Validator.TryValidateObject(query, new ValidationContext(query), expectedErrors))
            {
                throw new DomainError(expectedErrors.Select(e => e.ErrorMessage).ToList());
            }
        }
    }
}
