﻿using System.Threading.Tasks;

namespace EtheriumTransactionSearch.Domain.Handlers
{
    public interface IQueryHandler<TQuery, TReturn>
    {
        Task<TReturn> Execute(TQuery query);
    }
}
