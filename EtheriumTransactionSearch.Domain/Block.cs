﻿using System.Collections.Generic;

namespace EtheriumTransactionSearch.Domain
{
    public class Block
    {
        public List<BlockTransaction> Transactions { get; set; }
    }
}
