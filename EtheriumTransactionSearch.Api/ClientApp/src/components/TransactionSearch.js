import React, { useState } from 'react';


export const TransactionSearch = (props) => {
    const [blockNumber, setBlockNumber] = useState('0x5BAD55');
    const [address, setAddress] = useState(null);
    const [transactions, setTransactions] = useState([]);
    const [error, setError] = useState(null);

    const searchTransaction = async () => {
        const response = await fetch(`transactions/${blockNumber}?address=${address || ''}`);
        if (response.status != 200) {
            setError('Fail to fetch data');
            setTransactions([]);
        } else {
            const data = await response.json();
            setTransactions(data);
        }
    }

    return (
        <div>
            <div>
                <table>
                    <tbody>
                        <tr>
                            <td>Block Number</td>
                            <td><input type='text' value={blockNumber} onChange={(event) => setBlockNumber(event.target.value.trim())} /></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><input type='text' onChange={(event) => setAddress(event.target.value.trim())} /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><button type='button' onClick={searchTransaction} disabled={!blockNumber}>Search</button></td>
                        </tr>
                    </tbody>
                </table>              
            </div>

            {error && <p style={{ color: 'red' }}>{error}</p>}

            <div>
                <table className='table table-striped' aria-labelledby="tabelLabel">
                    <thead>
                        <tr>
                            <th>Block Hash</th>
                            <th>Block Number</th>
                            <th>Gas</th>
                            <th>Hash</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        {transactions.map(t =>
                            <tr>
                                <td>{t.blockHash}</td>
                                <td>{t.blockNumber}</td>
                                <td>{t.gas}</td>
                                <td>{t.hash}</td>
                                <td>{t.from}</td>
                                <td>{t.to}</td>
                                <td>{t.value}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
         </div>
    )
}
