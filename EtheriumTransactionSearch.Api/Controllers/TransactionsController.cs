﻿using EtheriumTransactionSearch.Domain;
using EtheriumTransactionSearch.Domain.Handlers;
using EtheriumTransactionSearch.Domain.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EtheriumTransactionSearch.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionsController : ControllerBase
    {
        private IQueryHandler<BlockTransactionsQuery, List<BlockTransaction>> queryHandler;

        public TransactionsController(IQueryHandler<BlockTransactionsQuery, List<BlockTransaction>> queryHandler)
        {
            this.queryHandler = queryHandler;
        }

        [HttpGet("{blockNumber}")]
        public async Task<IActionResult> SearchTransactions([FromRoute] string blockNumber, [FromQuery] string address)
        {            
            var transactions = await queryHandler.Execute(new BlockTransactionsQuery
            {
                BlockNumber = blockNumber,
                Address = address
            });            

            return new OkObjectResult(transactions);
        }
    }
}
