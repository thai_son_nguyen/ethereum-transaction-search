using EtheriumTransactionSearch.Domain.Errors;
using FluentAssertions;
using FluentAssertions.Execution;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace EtheriumTransactionSearch.Repository.Test
{
    public class InfuraRepositoryTests
    {
        public class GetBlockByNumber
        {

            [Fact]
            public async Task ShouldSendRequestToApi()
            {
                var mockMessageHandler = new Mock<HttpMessageHandler>();

                var response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(File.ReadAllText("GetBlockByNumberResponse.json")),
                };

                HttpRequestMessage request = null;

                mockMessageHandler.Protected()
                    .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                    .ReturnsAsync(response)
                    .Callback<HttpRequestMessage, CancellationToken>((message, _) => request = message);
                
                var url = "http://google.com";
                var httpClient = new HttpClient(mockMessageHandler.Object)
                {
                    BaseAddress = new Uri(url)
                };
                var repo = new InfuraRepository(httpClient);
                var blockNumber = "Asdasd";
                var expectedRequestContent = JObject.FromObject(new
                {
                    jsonrpc = "2.0",
                    method = "eth_getBlockByNumber",
                    Params = new List<object> { blockNumber, true },
                    id = 1
                }).ToString();


                var result = await repo.Get(blockNumber);

                using (new AssertionScope())
                {
                    request.Should().NotBeNull();
                    request.Method.Should().Be(HttpMethod.Post);
                    request.RequestUri.OriginalString.Should().Be(url);
                    request.Content.Headers.ContentType.MediaType.Should().Be("application/json");
                    (await request.Content.ReadAsStringAsync()).Should().Be(expectedRequestContent);
                }
            }

            [Fact]
            public async Task ShouldReturnResponse()
            {
                var mockMessageHandler = new Mock<HttpMessageHandler>();
                var testResponse = File.ReadAllText("GetBlockByNumberResponse.json");

                var response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(testResponse),
                };

                HttpRequestMessage request = null;

                mockMessageHandler.Protected()
                    .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                    .ReturnsAsync(response)
                    .Callback<HttpRequestMessage, CancellationToken>((message, _) => request = message);

                var url = "http://google.com";
                var httpClient = new HttpClient(mockMessageHandler.Object)
                {
                    BaseAddress = new Uri(url)
                };
                var repo = new InfuraRepository(httpClient);
                var blockNumber = "Asdasd";
                var expectedBlock = JsonConvert.DeserializeObject<GetBlockByNumberResponse>(await response.Content.ReadAsStringAsync());

                var result = await repo.Get(blockNumber);

                using (new AssertionScope())
                {
                    result.Transactions.Should().BeEquivalentTo(expectedBlock.Result.Transactions);
                }
            }

            [Fact]
            public void WhenThereIsErrorShouldThrowDomainError()
            {
                var mockMessageHandler = new Mock<HttpMessageHandler>();                                

                mockMessageHandler.Protected()
                    .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                    .Throws(new Exception());

                var url = "http://google.com";
                var httpClient = new HttpClient(mockMessageHandler.Object)
                {
                    BaseAddress = new Uri(url)
                };
                var repo = new InfuraRepository(httpClient);
                var blockNumber = "Asdasd";

                var expectedErrors = new List<string> { "Failed to load data" };

                repo.Awaiting(r => r.Get(blockNumber)).Should().Throw<DomainError>().And.Errors.Should().Equal(expectedErrors);                
            }

            [Fact]
            public void WhenRequestIsNotSuccessfulShouldThrowDomainError()
            {
                var mockMessageHandler = new Mock<HttpMessageHandler>();

                var response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.BadGateway
                };

                mockMessageHandler.Protected()
                    .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                    .ReturnsAsync(response);

                var url = "http://google.com";
                var httpClient = new HttpClient(mockMessageHandler.Object)
                {
                    BaseAddress = new Uri(url)
                };
                var repo = new InfuraRepository(httpClient);
                var blockNumber = "Asdasd";

                var expectedErrors = new List<string> { "Failed to load data" };

                repo.Awaiting(r => r.Get(blockNumber)).Should().Throw<DomainError>().And.Errors.Should().Equal(expectedErrors);
            }

            [Fact]
            public async Task WhenBlockIsNotFoundShouldReturnNull()
            {
                var mockMessageHandler = new Mock<HttpMessageHandler>();
                var testResponse = File.ReadAllText("GetBlockByNumberResponseNotFound.json");

                var response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(testResponse),
                };

                mockMessageHandler.Protected()
                    .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                    .ReturnsAsync(response);

                var url = "http://google.com";
                var httpClient = new HttpClient(mockMessageHandler.Object)
                {
                    BaseAddress = new Uri(url)
                };
                var repo = new InfuraRepository(httpClient);
                var blockNumber = "Asdasd";

                var result = await repo.Get(blockNumber);

                using (new AssertionScope())
                {
                    result.Should().BeNull();
                }
            }
        }
    }
}
